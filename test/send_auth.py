#!/usr/bin/env python
import pika

credentials = pika.PlainCredentials('fisi', 'fisi')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='ec2-18-221-252-55.us-east-2.compute.amazonaws.com', port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='auth')
channel.basic_publish(exchange='', routing_key='auth', body='{"card": "4213-5500-3333-2222", "pass": "fisi"}')

connection.close()
