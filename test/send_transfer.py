#!/usr/bin/env python
import pika

credentials = pika.PlainCredentials('fisi', 'fisi')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='ec2-18-221-252-55.us-east-2.compute.amazonaws.com', port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='transfer')
channel.basic_publish(exchange='', routing_key='transfer', 
    body='{"sender_id": 1, "receiver_id": 2, "user_id": 1, "token": "c6d876bb-961a-466e-9205-32a25eb6bd00", "amount": 200}')

connection.close()
