with import <nixpkgs>{};
stdenv.mkDerivation {
  name = "ezpay-backend";
  buildInputs = [
    python27Packages.pika
    python27Packages.requests
    ruby
    bundler
  ];
  shellHook = "mkdir -p .gems && export GEM_HOME=$(pwd)/.gems";
}
