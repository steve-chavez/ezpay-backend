#!/usr/bin/env python
import pika
import requests
import json

HOST = 'ec2-18-221-252-55.us-east-2.compute.amazonaws.com'

credentials = pika.PlainCredentials('fisi', 'fisi')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=HOST, port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='transfer')
channel.queue_declare(queue='transfer_error')
channel.queue_declare(queue='pagador')

def callback(ch, method, properties, body):
    print body
    b = json.loads(body)
    response = requests.get('http://{}:3000/users?id=eq.{}&token=eq.{}'.format(HOST, b['user_id'], b['token']),
            body, headers={'Accept': 'application/vnd.pgrst.object+json'})
    # raw_input("Press Enter to continue...")
    print response.text
    if response.status_code == 200:
        print "Token y cuenta validos"
        channel.basic_publish(exchange='', routing_key='pagador', body=body)
    else:
        message = "Token o cuenta invalida"
        print message
        channel.basic_publish(exchange='', routing_key='transfer_error', body=message)

# https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-ack
channel.basic_consume(callback, 'transfer', True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()

connection.close()
