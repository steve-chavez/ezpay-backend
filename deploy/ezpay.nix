{
  network.description = "ezpay server";

  ez = { config, pkgs, resources, ... }:
  let
    pgrst	= import ./pgrst.nix { stdenv = pkgs.stdenv; fetchurl = pkgs.fetchurl; };
  in
  {
		environment.systemPackages = [ pgrst ];

		services.rabbitmq = {
			enable = true;
			plugins = [ "rabbitmq_management" ];
			listenAddress = "::";
			cookie = builtins.readFile ./rmq-cookie.secret;
		};
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkOverride 10 ''
        local   all all trust
        host    all all 127.0.0.1/32 trust
        host    all all ::1/128 trust
      '';
      extraConfig = ''
        timezone = 'America/Lima'
      '';
    };
    systemd.services.postgrest = {
      enable      = true;
      description = "postgrest daemon";
      after    = [ "postgresql.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig.ExecStart = "${pgrst}/bin/postgrest ${./pgrst-ezpay.conf}";
    };

    networking.firewall.allowedTCPPorts = [ 5672 80 15672 3000 ];
	};
}
