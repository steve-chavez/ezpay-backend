-- createdb ezpay
begin;

create extension "uuid-ossp";

create table users(
  id    serial primary key
, name  text
, card  text
, pass  text
, token uuid
);

create table accounts(
  id      serial primary key
, name    text
, balance money
, user_id bigint references users
);

create table transactions(
  id          serial primary key
, sender_id   bigint references accounts
, receiver_id bigint references accounts
, amount      money
, "date"      timestamptz   default now()
);

create or replace function login(card text, pass text) returns users as $$
  update users set token = uuid_generate_v4()
  where card = $1 and pass = $2
  returning *;
$$ language sql;

create or replace function transfer(sender_id bigint, receiver_id bigint, amount integer) returns transactions as $$
  with
  send as (
    update accounts set balance = balance - amount::money
    where id = sender_id
  ),
  receive as (
    update accounts set balance = balance + amount::money
    where id = receiver_id
  )
  insert into transactions(sender_id, receiver_id, amount)
                   values (sender_id, receiver_id, amount::money)
  returning *;
$$ language sql;

copy users (id, name, card, pass) from stdin delimiter '|';
1|Diego Iturrizaga|4213-5500-3333-2222|fisi
2|David Nuñez|4444-1111-6666-7777|fisi
\.

copy accounts (id, name, balance, user_id) from stdin delimiter '|';
1|Cuenta soles|1500|1
2|Cuenta soles|3000|2
\.

commit;
