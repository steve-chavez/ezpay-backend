let
  region = "us-east-2";
  accessKeyId = "dev";

  ec2 =
    { resources, ... }:
    { deployment.targetEnv = "ec2";
      deployment.ec2 = {
        accessKeyId = accessKeyId;
        region = region;
        instanceType = "t2.micro";
        ebsInitialRootDiskSize = 30;
        associatePublicIpAddress = true;
        keyPair = resources.ec2KeyPairs.my-key-pair;
      };
    };

in
{
  ez = ec2;
  resources.ec2KeyPairs.my-key-pair = { inherit region accessKeyId; };
}
