#!/usr/bin/env python
import pika
import requests

credentials = pika.PlainCredentials('fisi', 'fisi')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='ec2-18-221-252-55.us-east-2.compute.amazonaws.com', port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='auth')

channel.queue_declare(queue='auth_success')

def callback(ch, method, properties, body):
    print body
    response = requests.post('http://ec2-18-221-252-55.us-east-2.compute.amazonaws.com:3000/rpc/login?select=id,name,token,accounts(name,balance)', 
            body, headers={'Accept': 'application/vnd.pgrst.object+json'})
    print response.text
    channel.basic_publish(exchange='', routing_key='auth_success', body=response.text)

# https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-ack
channel.basic_consume(callback, 'auth', True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()

connection.close()
