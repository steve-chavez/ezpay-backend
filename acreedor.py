#!/usr/bin/env python
import pika
import requests
import json

credentials = pika.PlainCredentials('fisi', 'fisi')
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='ec2-18-221-252-55.us-east-2.compute.amazonaws.com', port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='acreedor')
channel.queue_declare(queue='transfer_error')
channel.queue_declare(queue='transfer_success')

def callback(ch, method, properties, body):
    print body
    response = requests.post('http://ec2-18-221-252-55.us-east-2.compute.amazonaws.com:3000/rpc/transfer?columns=sender_id,receiver_id,amount', body)
    # raw_input("Press Enter to continue...")
    print response.text
    if response.status_code == 200:
        print "Transaccion completada exitosamente"
        channel.basic_publish(exchange='', routing_key='transfer_success', body=response.text)
    else:
        message = "Error inesperado"
        print message
        channel.basic_publish(exchange='', routing_key='transfer_error', body=message)

# https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-ack
channel.basic_consume(callback, 'acreedor', True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()

connection.close()
