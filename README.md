# ezpay backend

Prueba de concepto de orequestacion de procesos de negocio usando MOM.

![](imagen.png)

## Modulos
Primero se deben levantar los sigs modulos:

### Pagador en Ruby

Primero instalar ruby https://rubyinstaller.org, luego:

```
gem install unirest
gem install bunny
```

Correr el programa:

```
ruby pagador.rb
```

### Transferencias, Acreedor y Authenticacion en Python

Instalar python y pip, luego:

```
pip install pika
pip install requests
```

Correr ambos programas en diferentes terminales:

```
python transfer.py
python acreedor.py
python auth.py
```

## Flujo

Primero autenticarse:

```
python test/send_auth.py
```

Luego hacer la transferencia:

```
python test/send_transfer.py
```
