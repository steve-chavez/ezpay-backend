#!/usr/bin/env ruby

require 'bunny'
require 'unirest'
require 'json'

HOST = 'ec2-18-221-252-55.us-east-2.compute.amazonaws.com'

connection = Bunny.new(user: 'fisi', pass: 'fisi', 
                       hostname: HOST, automatically_recover: false)
connection.start

channel = connection.create_channel

pagador = channel.queue('pagador')
acreedor = channel.queue('acreedor')
transfer_error = channel.queue('transfer_error')

begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  pagador.subscribe(block: true) do |_delivery_info, _properties, body|
    puts " [x] Received #{body}"
    b = JSON.parse(body)
    response = Unirest.get "http://#{HOST}:3000/accounts?id=eq.#{b['sender_id']}&balance=gte.#{b['amount']}", 
                            headers:{ "Accept" => "application/vnd.pgrst.object+json" }
    puts response.raw_body
    if response.code == 200
      puts "Cuenta con fondos suficientes"
      channel.default_exchange.publish(body, routing_key: acreedor.name)
    else
      message = "No cuenta con fondos suficientes"
      puts message
      channel.default_exchange.publish(message, routing_key: transfer_error.name)
    end
  end
rescue Interrupt => _
  connection.close
  exit(0)
end
